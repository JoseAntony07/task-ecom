from functools import wraps
from rest_framework import status, permissions
from rest_framework.response import Response
from ECOM.utils import get_cleaned_permissions


def has_permissions(names):
    def decorator(func):
        @wraps(func)
        def wrapper(obj, *args, **kwargs):
            user = obj.request.user
            if user.is_superuser or 'Owner' in user.types or 'Manager' in user.types or user.category == 'FlipKart':
                return func(obj, *args, **kwargs)

            if [True for i in list(get_cleaned_permissions(user.get_user_permissions())) if i == 'add_group']:
                return func(obj, *args, **kwargs)

            # if get_cleaned_permissions(user.get_user_permissions())
            message = f"You don't have any of the following permissions: ({', '.join(names)})."
            return Response(
                {'error': message},
                status=status.HTTP_403_FORBIDDEN
            )
        return wrapper
    return decorator


class IsAdministrator(permissions.BasePermission):
    """
    permission to only allow admin or superuser of the project to edit it.
    """
    def has_permission(self, request, view):
        if request.user and request.user.is_authenticated:
            return bool(request.user.category == 'Flipkart' or request.user.is_superuser)

        return False


class IsSeller(permissions.BasePermission):
    def has_permission(self, request, view):
        return bool(request.user.category == 'Seller' and request.user.has_types(types=['Owner', 'Admin', 'Manager']))


class IsCustomer(permissions.BasePermission):
    def has_permission(self, request, view):
        return bool(request.user.category == 'Customer' and request.user.has_types(types=['Member']))


class PremiumUser(permissions.BasePermission):
    def has_permission(self, request, view):
        if [True for i in list(get_cleaned_permissions(request.user.get_group_permissions())) if i == 'view_premium_product']:
            return True
