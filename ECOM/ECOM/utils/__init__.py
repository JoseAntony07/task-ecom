from rest_framework.permissions import BasePermission
import jwt
from rest_framework.exceptions import AuthenticationFailed
from django.conf import settings
from datetime import datetime
# from ECOM.ECOM.Products.models import Tenant


def get_cleaned_permissions(permissions):
    # Remove app label name from permission string
    return {permission.split('.')[1] for permission in permissions}


class IsAuthenticated(BasePermission):
    """
    Allows access only to authenticated users.
    """

    def has_permission(self, request, view):
        def get_token():
            token = None

            # TODO if token passed as authorization bearer token
            if request.META.get('HTTP_AUTHORIZATION'):
                token = request.META.get('HTTP_AUTHORIZATION').split(' ')[1]

            # TODO if token get from cookie
            elif request.COOKIES.get('jwt'):
                token = request.COOKIES.get('jwt')

            return token

        if not get_token():
            raise AuthenticationFailed('Unauthenticated')

        # Customise the token expiration
        # payload = jwt.decode(token, 'secret', algorithms=['HS256'])
        # exp_time = datetime.strptime(payload.get('exp'), '%Y%m%d%H%M%S')
        # if exp_time < datetime.strptime(datetime.now().strftime('%Y-%m-%d %H:%M:%S'), '%Y-%m-%d %H:%M:%S'):
        #     raise AuthenticationFailed('Token Expired!')

        try:
            payload = jwt.decode(get_token(), settings.SECRET_KEY, algorithms=['HS256'])
        except jwt.ExpiredSignatureError:
            """
            1) this error will happen when token expiration time(we set in login api) is completed.
            """
            raise AuthenticationFailed('Token Expired....Please Login Again!')

        except jwt.InvalidSignatureError:
            """
            1) token --> this token has signature, signature has header + payload + secret key when we are decode it,
            if the above client sent token - signature - secret key is not matched with settings.SECRET_KEY, which means
            someone change or access our user token.
            
            2) Our settings.SECRET_KEY and client token signature secret key are
            not matched.so that signature is invalid.
            """
            raise AuthenticationFailed('InvalidSignature....Someone Hacked!')

        try:
            from Products.models import User
            user = User.objects.get(id=payload['id'])
            print(user)
            request.user = user

        except Exception as e:
            print(e)
            raise AuthenticationFailed('Unauthenticated!')

        return bool(user.is_authenticated)


def apply_product_filter(queryset, _filter):
    if _filter == 'low_to_high':
        queryset = queryset.order_by('price')

    elif _filter == 'high_to_low':
        queryset = queryset.order_by('-price')

    elif _filter == 'newest_first':
        queryset = queryset.order_by('-created_at')


    return queryset


def apply_product_query_filter(queryset, query):
    if query:
        query = eval(query)

        if brand := query.get('brand'):
            queryset = queryset.filter(brand__id__in=brand)

        if min_price := query.get('min_price'):
            queryset = queryset.filter(price__lt=min_price[0])

        if max_price := query.get('max_price'):
            queryset = queryset.filter(price__gt=max_price[0])

        if rating := query.get('rating'):
            queryset = queryset.filter(rating__in=rating)

    return queryset

