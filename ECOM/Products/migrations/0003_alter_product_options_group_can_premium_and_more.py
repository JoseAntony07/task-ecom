# Generated by Django 4.1.1 on 2022-10-24 14:41

import django.contrib.postgres.fields
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('Products', '0002_remove_group_can_added_job_remove_group_can_approve'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='product',
            options={'permissions': (('add_premium_product', 'Can Add Premium Product'), ('view_premium_product', 'Can View Premium Product'), ('change_premium_product', 'Can Change Premium Product'))},
        ),
        migrations.AddField(
            model_name='group',
            name='can_premium',
            field=django.contrib.postgres.fields.ArrayField(base_field=models.CharField(blank=True, max_length=20), blank=True, null=True, size=None),
        ),
        migrations.AlterField(
            model_name='permissionlabel',
            name='type',
            field=models.CharField(choices=[('View', 'View'), ('Add', 'Add'), ('Change', 'Change'), ('Delete', 'Delete'), ('Premium', 'Premium')], max_length=10, verbose_name='Type'),
        ),
    ]
