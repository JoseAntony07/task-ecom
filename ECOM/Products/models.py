from django.db.models import CheckConstraint, Q, UniqueConstraint
from django.core.validators import MaxValueValidator, MinValueValidator
import uuid
from django.db import models
from django.utils.translation import gettext_lazy as _
from rest_framework_simplejwt.tokens import RefreshToken
from django.contrib.auth.models import AbstractBaseUser, BaseUserManager, GroupManager, Permission, PermissionsMixin
from django.contrib.postgres.fields import ArrayField
from ECOM.utils import get_cleaned_permissions
# from django_tenants.models import TenantMixin, DomainMixin
# Create your models here.


class AbstractBaseModel(models.Model):
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    is_active = models.BooleanField(default=True)

    class Meta:
        abstract = True


class BaseModel(AbstractBaseModel):
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    created_by = models.ForeignKey('Products.User', verbose_name='Created by', on_delete=models.SET_NULL, editable=False,
                                   null=True, related_name="created_%(class)s_set")
    modified_by = models.ForeignKey('Products.User', verbose_name='Modified by', on_delete=models.SET_NULL, editable=False,
                                    null=True, related_name="modified_%(class)s_set")
    created_at = models.DateTimeField('Created at', auto_now_add=True)
    modified_at = models.DateTimeField('Modified at', auto_now=True)

    class Meta:
        abstract = True


class CustomUserManager(BaseUserManager):
    def _create_user(self, email, password, first_name, last_name, is_staff=False, is_superuser=False, **extra_fields):
        if not email:
            raise ValueError('Email must be provided')
        if not password:
            raise ValueError('Password is not provided')

        user = self.model(email=self.normalize_email(email),
                          password=password,
                          first_name=first_name,
                          last_name=last_name,
                          is_staff=is_staff,
                          is_superuser=is_superuser,
                          **extra_fields
                          )
        user.set_password(password)
        user.save(using=self._db)
        return user

    def create_user(self, email, password, first_name, last_name):
        return self._create_user(email, password, first_name, last_name)

    def create_superuser(self, email, password, **extra_fields):
        return self._create_user(email, password, True, True, **extra_fields)


class Tenant(models.Model):
    name = models.CharField(max_length=255)
    subdomain = models.CharField(max_length=255)


class PermissionLabel(AbstractBaseModel):
    """
    This is the admin configuration table for maintain UI level permissions operations
    """
    TYPES = (('View', 'View'), ('Add', 'Add'), ('Change', 'Change'), ('Delete', 'Delete'), ('Premium', 'Premium'))

    type = models.CharField(_('Type'), choices=TYPES, max_length=10)
    name = models.CharField(_('name'), max_length=30)
    permission = models.CharField(_('Permission'), max_length=50, unique=True)
    order = models.PositiveSmallIntegerField(_('Order'), default=1)

    class Meta:
        db_table = 'permission_label'
        verbose_name = _('Permission Label')
        verbose_name_plural = _('Permission Labels')
        # unique_together = ('type', 'name')
        ordering = ('order',)


class Group(BaseModel):
    name = models.CharField(_('name'), max_length=150)
    can_view = ArrayField(models.CharField(max_length=20, blank=True), blank=True, null=True)
    can_add = ArrayField(models.CharField(max_length=20, blank=True), blank=True, null=True)
    can_change = ArrayField(models.CharField(max_length=20, blank=True), blank=True, null=True)
    can_delete = ArrayField(models.CharField(max_length=20, blank=True), blank=True, null=True)
    can_premium = ArrayField(models.CharField(max_length=20, blank=True), blank=True, null=True)
    permissions = models.ManyToManyField(
        Permission,
        verbose_name=_('permissions'),
        blank=True,
        related_name='groups',
        related_query_name='custom_group'
    )

    objects = GroupManager()

    class Meta:
        db_table = 'group'
        verbose_name = _('Group')
        verbose_name_plural = _('Groups')

    def __str__(self):
        return self.name


class UserPermissionsMixin(PermissionsMixin):
    groups = models.ManyToManyField(
        Group,
        verbose_name=_('groups'),
        blank=True,
        help_text=_(
            'The groups this user belongs to. A user will get all permissions '
            'granted to each of their groups.'
        ),
        related_name='users',
        related_query_name='user_permissions'  # Do not change this name.
    )

    class Meta:
        abstract = True

    def _get_user_permissions(self):
        return self.user_permissions.all()

    def _get_group_permissions(self):
        return Permission.objects.filter(**{'custom_group__user_permissions': self})

    def _get_permissions(self, obj, from_name):
        """
        Return the permissions of `user_obj` from `from_name`. `from_name` can
        be either "group" or "user" to return permissions from
        `_get_group_permissions` or `_get_user_permissions` respectively.
        """
        if not self.is_active or self.is_anonymous or obj is not None:
            return set()

        perm_cache_name = '_%s_perm_cache' % from_name
        if not hasattr(self, perm_cache_name):
            if self.is_superuser:
                perms = Permission.objects.all()
            else:
                perms = getattr(self, '_get_%s_permissions' % from_name)()
            perms = perms.values_list('content_type__app_label', 'codename').order_by()
            setattr(self, perm_cache_name, {"%s.%s" % (ct, name) for ct, name in perms})
        return getattr(self, perm_cache_name)

    def _get_all_permissions(self, obj=None):
        return {
            *self.get_user_permissions(obj=obj),
            *self.get_group_permissions(obj=obj),
        }

    def get_user_permissions(self, obj=None):
        """
        Return a set of permission strings the user `user_obj` has from their
        `user_permissions`.
        """
        return self._get_permissions(obj, 'user')

    def get_group_permissions(self, obj=None):
        """
        Return a set of permission strings the user `user_obj` has from the
        groups they belong.
        """
        return self._get_permissions(obj, 'group')

    def get_all_permissions(self, obj=None):
        if not self.is_active or self.is_anonymous or obj is not None:
            return set()
        if not hasattr(self, '_perm_cache'):
            self._perm_cache = self._get_all_permissions()
        return self._perm_cache


class User(BaseModel, AbstractBaseUser, UserPermissionsMixin):
    CATEGORIES = (('Flipkart', 'Flipkart'), ('Seller', 'Seller'), ('Customer', 'Customer'))
    TYPES = (('Owner', 'Owner'), ('Admin', 'Admin'), ('Manager', 'Manager'), ('Member', 'Member'))
    GENDERS = (('Male', 'Male'), ('Female', 'Female'), ('Other', 'Other'))

    email = models.EmailField(_('email'), unique=True)
    first_name = models.CharField(_('first name'), max_length=150, blank=True)
    last_name = models.CharField(_('last name'), max_length=150, blank=True)
    address = models.TextField(_('Address'))
    gender = models.CharField(_('Gender'), choices=GENDERS, max_length=12, blank=True)
    category = models.CharField(_('Category'), choices=CATEGORIES, max_length=25, default='Flipkart')
    types = ArrayField(models.CharField(choices=TYPES, max_length=25), default=list, help_text=_('Use comma(,) separate to add multiple types.'))

    is_staff = models.BooleanField(_('staff status'), default=False, help_text=_('Designates whether the user can log into this admin site.'))
    is_active = models.BooleanField(_('active'), default=True, help_text=_('Unselect this instead of deleting accounts.'))
    is_superuser = models.BooleanField(_('superuser status'), default=False, help_text=_('Designates that this user has all permissions'))

    objects = CustomUserManager()

    USERNAME_FIELD = 'email'
    EMAIL_FIELD = 'email'

    class Meta:
        db_table = 'user'
        verbose_name = _('User')
        verbose_name_plural = _('Users')
        ordering = ('-created_at',)

    @property
    def full_name(self):
        return f'{self.first_name} {self.last_name}'.strip()

    def has_types(self, types):
        return any(_type in self.types for _type in types)

    def get_profile(self):
        fields = ['id', 'category', 'types', 'full_name', 'email', 'is_active']
        data = {f: getattr(self, f) for f in fields}
        data.update({'permissions': get_cleaned_permissions(self.get_all_permissions())})
        return data


class Category(models.Model):
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    name = models.CharField(max_length=200)

    def __str__(self):
        return self.name


class Brand(models.Model):
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    name = models.CharField(max_length=200)

    def __str__(self):
        return self.name


class Product(models.Model):
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    name = models.CharField(max_length=200)
    price = models.FloatField()
    category = models.ForeignKey(Category, on_delete=models.CASCADE, default=False, null=True)
    brand = models.ForeignKey(Brand, on_delete=models.CASCADE, default=False, null=True)
    description = models.TextField()
    created_at = models.DateTimeField('Created at', auto_now_add=True)
    rating = models.FloatField(validators=[MinValueValidator(0.0), MaxValueValidator(5.0)])
    is_premium = models.BooleanField(default=False)

    class Meta:
        constraints = [
            CheckConstraint(check=Q(rating__range=(0, 5)), name='valid_rate')
        ]
        permissions = (
            ('add_premium_product', 'Can Add Premium Product'),
            ('view_premium_product', 'Can View Premium Product'),
            ('change_premium_product', 'Can Change Premium Product')
        )

    def __str__(self):
        return self.name
