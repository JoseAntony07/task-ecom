from django.contrib import admin
from .models import Product, Brand, Category, User, Tenant, Group
from django.contrib.auth.admin import UserAdmin
from django.contrib.auth.models import Group as AuthGroup

admin.site.unregister(AuthGroup)
admin.site.register(Product)
admin.site.register(Brand)
admin.site.register(Category)
admin.site.register(Tenant)


@admin.register(User)
class UserAdmin(UserAdmin):
    model = User
    list_display = ('email', 'first_name', 'last_name', 'is_staff', 'is_active')
    list_filter = ('is_staff', 'is_superuser', 'is_active')
    fieldsets = (
        (None, {'fields': ('email', 'password')}),
        ('Personal info', {'fields': ('first_name', 'last_name', 'gender', 'category', 'types')}),
        ('Permissions', {'fields': ('is_active', 'is_staff', 'is_superuser', 'last_login', 'groups',
                                    'user_permissions'), }),
    )
    search_fields = ('email', 'first_name', 'last_name')
    ordering = ('email',)
    filter_horizontal = ('groups', 'user_permissions',)


@admin.register(Group)
class GroupAdmin(admin.ModelAdmin):
    list_display = ('name', 'is_active')
    search_fields = ('name',)
    fields = ('name', 'can_view', 'can_add', 'can_change', 'can_delete', 'can_premium', 'permissions', 'is_active')
    filter_horizontal = ('permissions',)
