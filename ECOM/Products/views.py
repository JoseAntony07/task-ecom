import json
from django.shortcuts import render
from datetime import timedelta, datetime
from rest_framework.viewsets import ModelViewSet, GenericViewSet
from .models import Category, Product, Brand, User, Tenant, Group, PermissionLabel
from .serializers import CategorySerializer, ProductSerializer, BrandSerializer, UserSerializer, GroupSerializer
from rest_framework.mixins import ListModelMixin, UpdateModelMixin
from rest_framework import filters, status
from rest_framework.exceptions import AuthenticationFailed
from rest_framework.response import Response
from ECOM.utils import apply_product_filter, apply_product_query_filter, IsAuthenticated
from ECOM.utils.permissions import has_permissions, IsAdministrator, IsSeller, IsCustomer, PremiumUser
from rest_framework.views import APIView
import jwt
from django.conf import settings
from rest_framework.decorators import action
from django.core.serializers.json import DjangoJSONEncoder
from django.contrib.auth.mixins import PermissionRequiredMixin
from django.contrib.auth.decorators import permission_required
from django.views.generic import ListView
from django.http import JsonResponse
from django.views.generic import View
from rest_framework.throttling import UserRateThrottle
from django.views.decorators.cache import cache_page


class TestAPI(APIView):
    def get(self, request):
        return Response({'message': 'successfully deployed project using gitlab ci/cd by jose....!'})

class UserLoginView(ListView):
    model = User
    template_name = 'login.html'
    context_object_name = 'user_list'


def get_hostname(request):
    print(request.get_host())
    return request.get_host().split('.')[0]


def get_tenant(request):
    hostname = get_hostname(request)
    subdomain = hostname.split('.')[0]
    print(subdomain, 'subdomain==========')
    return Tenant.objects.filter(subdomain=subdomain).first()


def our_team(request):
    tenant = get_tenant(request)
    print(tenant)
    return render(request, 'our_team.html', {'tenant': tenant})


class RegisterAPI(APIView):
    def post(self, request):
        print(request, '========= request ===========')
        print(request.data,  '==============================')
        serializer = UserSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        print('serializer is valid ===========')
        serializer.save()
        return Response(serializer.data)


class LoginAPI(APIView):
    def post(self, request):
        email = request.data['email']
        password = request.data['password']

        user = User.objects.filter(email=email).first()

        if user is None:
            raise AuthenticationFailed('User Not Found!')

        if not user.check_password(password):
            raise AuthenticationFailed('Incorrect Password!')

        # jwt_expiration_time
        jwt_expiration_time = datetime.utcnow() + timedelta(minutes=1)

        # jwt payload(header, payload, secret)
        payload = {
            'id': str(user.id),
            'exp': jwt_expiration_time,
            'iat': jwt_expiration_time.microsecond
        }

        # TODO JWT token creation
        """
        JWT TOKEN = 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6IjdmYzFlNjEwLWZlNWQtNDJiYy05ZGMzLWJkMWQ4MTI4YjE5MyIsImV4cCI6IjIwMjMwNDAzMTM0MTAxIiwiaWF0IjoyODM5NDd9.lFgmwo2ZQVVYPUqQvVIfgSavPCAScZObfMbF-Wgl7uA'
        
        eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9 --> Header(base64 encoded) --> {"alg": "HS256", "typ": "JWT"}
        
        eyJpZCI6IjdmYzFlNjEwLWZlNWQtNDJiYy05ZGMzLWJkMWQ4MTI4YjE5MyIsImV4cCI6IjIwMjMwNDAzMTM0MTAxIiwiaWF0IjoyODM5NDd9 --> Payload(base64 encoded) --> {"id": "1f0ba098-2bfb-4ade-bf46-b0db50f00682", "exp": "20230403141644", "iat": 243454}
        
        lFgmwo2ZQVVYPUqQvVIfgSavPCAScZObfMbF-Wgl7uA --> Signature(HMACSHA256(Header(base64 encoded)+'.'+Payload(base64 encoded)+'.'+settings.SECRET_KEY(base64 encoded)))
        
         """
        token = jwt.encode(payload, settings.SECRET_KEY, algorithm='HS256').encode().decode('utf-8')

        # in the above jwt token, we have to set it as cookie
        response = Response()

        # TODO if we don't use cookies, simply hide below line.then it works like simple jwt
        response.set_cookie(key='jwt', value=token, httponly=True)  #httponly - we don't want frontend to access our token
        response.data = {"jwt": token}

        return response


class UserView(APIView):  # get authenticated user using cookie
    def get(self, request):
        token = request.COOKIES.get('jwt')

        if not token:
            raise AuthenticationFailed('Unauthenticated!')

        try:
            # verify the jwt token
            # No one can decode the jwt token until they don't have server secret key(mainly can't decode signature)
            # because in signature part they need server secret key
            # and signature is hashed,so if we're trying to decode. it tells (i.e:- not a UTF-8 string)
            payload = jwt.decode(token, settings.SECRET_KEY, algorithms=['HS256'])
            print(payload, '====payload')
        except jwt.ExpiredSignatureError:
            raise AuthenticationFailed('Unauthenticated!')

        user = User.objects.filter(id=payload['id']).first()
        serializer = UserSerializer(user)
        return Response(serializer.data)


class LogoutView(APIView):
    def post(self, request):
        response = Response()
        response.delete_cookie('jwt')
        response.data = {"message": "success"}
        return response


class UserViewSet(ModelViewSet):
    serializer_class = UserSerializer
    permission_classes = (IsAuthenticated,)
    filter_backends = [filters.SearchFilter]
    search_fields = ['first_name', 'last_name', 'email']

    @action(detail=False, methods=['get'], url_path='profile')
    def profile(self, request):
        data = request.user.get_profile()
        return Response(data)

    @action(detail=True, methods=['patch'], url_path='add_user_to_group')
    def add_user_to_group(self, request, pk=None):
        user = request.user
        group = Group.objects.get(id=pk)
        user.groups.add(group)
        return Response({"messege": "{user} Successfully added to this group {group}".format(user=user, group=group)})


# class CategoryViewSet(PermissionRequiredMixin, ModelViewSet):
class CategoryViewSet(ModelViewSet):
    permission_classes = [IsAuthenticated, IsSeller]
    serializer_class = CategorySerializer
    queryset = Category.objects.all()
    filter_backends = [filters.SearchFilter]
    search_fields = ['id', 'name']


class BrandViewSet(ModelViewSet):
    permission_classes = [IsAuthenticated, IsSeller]
    serializer_class = BrandSerializer
    queryset = Brand.objects.all()
    filter_backends = [filters.SearchFilter]
    search_fields = ['id', 'name']


class ProductViewSet(ModelViewSet):
    permission_classes = [IsAuthenticated]
    # permission_required = ('view_premium_product',)
    throttle_classes = [UserRateThrottle]
    serializer_class = ProductSerializer
    filter_backends = [filters.SearchFilter]
    search_fields = ['id', 'name', 'price', 'category__name', 'description', 'created_at', 'brand__name', 'rating']

    def get_queryset(self):
        queryset = Product.objects.select_related('category', 'brand').filter(is_premium=False)

        if _filter := self.request.query_params.get('filter'):
            queryset = apply_product_filter(queryset, _filter)

        if query := self.request.query_params.get('query'):
            queryset = apply_product_query_filter(queryset, query)

        return queryset

    # @cache_page(60 * 15)
    def list(self, request, *args, **kwargs):
        print(request.user)
        queryset = self.filter_queryset(self.get_queryset())

        page = self.paginate_queryset(queryset)
        if page is not None:
            serializer = self.get_serializer(page, many=True)
            return self.get_paginated_response(serializer.data)

        serializer = self.get_serializer(queryset, many=True)
        return Response(serializer.data)

    @action(detail=False, methods=['get'], permission_classes=[IsAuthenticated, PremiumUser], url_path='premium-product')
    def premium_product(self, request):
        queryset = Product.objects.select_related('category', 'brand').filter(is_premium=True)
        page = self.paginate_queryset(queryset)
        serializer = self.get_serializer(page, many=True)  # many=True - return list of objects
        return self.get_paginated_response(serializer.data)


# @permission_required("product.view_premium_product")
# def premium_product(self, request):
#     queryset = Product.objects.select_related('category', 'brand').filter(is_premium=True)
#     page = self.paginate_queryset(queryset)
#     serializer = self.get_serializer(page, many=True)
#     return self.get_paginated_response(serializer.data)


class GroupViewSet(ModelViewSet):
    serializer_class = GroupSerializer
    permission_classes = [IsAuthenticated]
    filter_backends = [filters.SearchFilter]
    search_fields = ['name']

    def get_queryset(self):
        queryset = Group.objects.prefetch_related('permissions').all()
        return queryset

    @has_permissions(['add_group'])
    def create(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        self.perform_create(serializer)
        headers = self.get_success_headers(serializer.data)
        return Response(serializer.data, status=status.HTTP_201_CREATED, headers=headers)


class PermissionLabelViewSet(GenericViewSet, ListModelMixin):
    queryset = PermissionLabel.objects.filter(is_active=True)
    permission_classes = (IsAuthenticated,)
    pagination_class = None

    def list(self, request, *args, **kwargs):
        queryset = self.get_queryset()

        data = dict()
        for _type in ('View', 'Add', 'Change', 'Delete', 'Premium'):
            permissions = queryset.filter(type=_type)
            if permissions.exists():
                data.update(
                    {
                        _type.lower(): [perm.name for perm in permissions]
                    }
                )

        return Response(data)
