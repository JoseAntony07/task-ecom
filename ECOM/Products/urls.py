from django.urls import path, include
from rest_framework.routers import DefaultRouter
from .views import CategoryViewSet, ProductViewSet, BrandViewSet, RegisterAPI, LoginAPI, UserView, LogoutView, our_team, \
    GroupViewSet, PermissionLabelViewSet, UserViewSet, UserLoginView, TestAPI

# from ECOM.utils.permissions import has_permissions, IsAdministrator, IsSeller, IsCustomer, PremiumUser
# from django.contrib.auth.decorators import login_required, permission_required


router = DefaultRouter()
router.register(r'category', CategoryViewSet, 'category')
router.register(r'product', ProductViewSet, 'product')
router.register(r'brand', BrandViewSet, 'brand')
router.register(r'groups', GroupViewSet, 'groups')
router.register(r'permission-labels', PermissionLabelViewSet, 'permission_labels')
router.register(r'get_user', UserViewSet, 'get_user')

urlpatterns = [
    path('', include(router.urls)),
    path('api-test/',  TestAPI.as_view(), name='test-api'),
    path('user-login/',  UserLoginView.as_view(), name='user-login'),
    path('our_team/', our_team, name='our_team'),
    path('register/', RegisterAPI.as_view(), name='user_register'),
    path('login/', LoginAPI.as_view()),
    path('user/', UserView.as_view()),
    path('logout/', LogoutView.as_view()),
    # path('premium/', premium_product),
    # path('premium/', permission_required('product.view_premium_product')(PermissionLabelViewSet))
]
