from .models import Category, Product, Brand, User, Group, PermissionLabel
from rest_framework import serializers
from django.utils.translation import gettext_lazy as _
from django.contrib.auth.models import Permission


class UserSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ('id', 'first_name', 'last_name', 'gender', 'email', 'password', 'is_staff', 'is_superuser', 'category',
                  'types', 'groups')
        extra_kwargs = {'password': {'write_only': True}}

    def create(self, validated_data):
        print('its enter')
        print(validated_data, '============ validated data ==========')
        password = validated_data.pop('password', None)
        instance = self.Meta.model(**validated_data)  # without password
        if password is not None:
            instance.set_password(password)
        instance.save()
        return instance


class CategorySerializer(serializers.ModelSerializer):
    class Meta:
        model = Category
        fields = ('id', 'name')


class BrandSerializer(serializers.ModelSerializer):
    class Meta:
        model = Brand
        fields = ('id', 'name')


class ProductSerializer(serializers.ModelSerializer):
    category_ = serializers.SerializerMethodField(label='Category')
    brand_ = serializers.SerializerMethodField(label='Brand')

    class Meta:
        model = Product
        fields = ('id', 'name', 'price', 'category', 'description', 'created_at', 'brand', 'rating',
                  'is_premium', 'brand_', 'category_')

    def get_category_(self, obj):
        return {'id': obj.category.id, 'name': obj.category.name}

    def get_brand_(self, obj):
        return {'id': obj.brand.id, 'name': obj.brand.name}


class GroupSerializer(serializers.ModelSerializer):
    permissions = serializers.StringRelatedField(many=True, read_only=True)

    class Meta:
        model = Group
        fields = ('id', 'name', 'can_view', 'can_add', 'can_change', 'can_premium', 'permissions')

    def validate(self, attrs):
        if not (attrs.get('can_view') or attrs.get('can_add') or attrs.get('can_premium') or attrs.get('can_change')):
            raise serializers.ValidationError({'error': "You have to select at least one permission to this Role."})
        return attrs

    def create(self, validated_data):
        request = self.context.get('request')

        validated_data['created_by'] = request.user
        validated_data['permissions'] = self.get_permissions(validated_data)

        return super().create(validated_data)

    def update(self, instance, validated_data):
        instance.modified_by = self.context.get('request').user
        validated_data['permissions'] = self.get_permissions(validated_data)

        return super().update(instance, validated_data)

    def get_permissions(self, validated_data):
        values = list()
        if validated_data.get('can_add'):
            for val in validated_data.get('can_add'):
                values.extend(PermissionLabel.objects.filter(is_active=True, type='Add', name=val).values_list('permission', flat=True))

        if validated_data.get('can_change'):
            for val in validated_data.get('can_change'):
                values.extend(PermissionLabel.objects.filter(is_active=True, type='Change', name=val).values_list('permission', flat=True))

        if validated_data.get('can_view'):
            for val in validated_data.get('can_view'):
                values.extend(PermissionLabel.objects.filter(is_active=True, type='View', name=val).values_list('permission', flat=True))

        if validated_data.get('can_premium'):
            for val in validated_data.get('can_premium'):
                values.extend(PermissionLabel.objects.filter(is_active=True, type='Premium', name=val).values_list('permission', flat=True))

        if values:
            return Permission.objects.filter(codename__in=values).values_list('id', flat=True)

        return list()

