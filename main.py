# This is a sample Python script.

# Press Shift+F10 to execute it or replace it with your code.
# Press Double Shift to search everywhere for classes, files, tool windows, actions, and settings.


def print_hi(name):
    # Use a breakpoint in the code line below to debug your script.
    print(f'Hi, {name}')  # Press Ctrl+F8 to toggle the breakpoint.


# Press the green button in the gutter to run the script.
if __name__ == '__main__':
    print_hi('PyCharm')

# See PyCharm help at https://www.jetbrains.com/help/pycharm/

# ============================================================
import pytracking


open_tracking_url = pytracking.get_open_tracking_url(
    {"customer_id": 1}, base_open_tracking_url="https://trackingdomain.com/path/",
    webhook_url="http://requestb.in/123", include_webhook_url=True)

print(open_tracking_url)
