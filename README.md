<<<<<<< README.md
# flipkart



## Models

Product - It contains all category and all brand products

Category - All product categories defined in this model

Brand - All product brand defined in this model



## End points

GET

1) All Product Search - http://127.0.0.1:8000/api/product/?search=oakter

2)  i) All Product Sorting high_to_low - http://127.0.0.1:8000/api/product/?filter=high_to_low
    
    ii) All Product Sorting low_to_high - http://127.0.0.1:8000/api/product/?filter=low_to_high

    iii) All Product Sorting newest_first - http://127.0.0.1:8000/api/product/?filter=newest_first

3) Multiple Filter And Sorting

   i) max_price - http://127.0.0.1:8000/api/product/?query={%22max_price%22:[600],%22brand%22:[1,2]}&filter=high_to_low
      
      Explanation - max_price = above 600
                 
                  - brand = [1,2] (brand_id)
                  
                  - high_to_low = desc ordering
   
   ii) min_price - http://127.0.0.1:8000/api/product/?query={%min_price%22:[600],%22brand%22:[1,2]}&filter=high_to_low

   iii) rating - http://127.0.0.1:8000//api/product/?query={%22max_price%22:[600],%22brand%22:[1], %22rating%22:[3,3.5,4.0,5]}&filter=high_to_low

      Explanation - max_price = above 600
                 
                  - brand = [1] (brand_id)
                  
                  - high_to_low = desc ordering

                  - rating = [3,3.5,4.0,4.5,5] (3&above => 3,3.5,4.0,4.5,5.0)
CREATE

1) Create Brand - http://127.0.0.1:8000/api/brand/

    Body - {
             "name": "Oakter"
           }

2) Create Category - http://127.0.0.1:8000/api/category/

    Body - {
             "name": "Plug"
           }

3) Create Product - http://127.0.0.1:8000/api/product/

    Body - {
             "name": "Oakter Oakplug Combo Smart Plug",
             "price": 1200.0,
             "category": 2,
             "brand": 2,
             "description": "high quality product"
          }

## DataBase

   Postgres - {
       'default': {
        'ENGINE': 'django.db.backends.postgresql_psycopg2',
        'NAME': 'flipkart',
        'USER': 'postgres',
        'PASSWORD': 'jose07',
        'HOST': '127.0.0.1',
        'PORT': '5432'}
       }


## handle jwt in two ways

1) jwt token get from cookie
2) token passed as authorization bearer token